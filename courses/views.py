from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status

from .models import Course, Lesson, Media, Attendance
from .serializers import CourseSerializer, LessonSerializer, SubmissionSerializer, MediaSerializer, CourseDeatailSerializer
from .permissions import IsAdminOrReadOnly

class CoursesViewSet(viewsets.ModelViewSet):
    queryset = Course.objects.all()
    permission_classes = (IsAdminOrReadOnly, )

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return CourseDeatailSerializer
        return CourseSerializer


class LessonViewSet(viewsets.ModelViewSet):
    serializer_class = LessonSerializer
    permission_classes = (IsAdminOrReadOnly, )

    def get_queryset(self):
        return Lesson.objects.filter(course=self.kwargs['course_pk'])

    def retrieve(self, request, pk, course_pk):
        Attendance.objects.get_or_create(user=request.user, lesson_id=pk)
        return super(LessonViewSet, self).retrieve(self, request, pk, course_pk)

    def create(self, request, course_pk):
        data = request.data.copy()
        data['course'] = course_pk
        serializer = LessonSerializer(data=data, context={'request':request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk, course_pk, partial=True):
        data = request.data.copy()
        data['course'] = course_pk
        serializer = LessonSerializer(self.get_object(), data=data, context={'request':request}, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=['post'])
    def submit(self, request, pk=None, **kwargs):
        data = request.data.copy()
        data['lesson'] = self.get_object().pk
        data['user'] = request.user.pk
        serializer = SubmissionSerializer(data=data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=['post'])
    def media(self, request, pk=None, **kwargs):
        data = request.data.copy()
        for media in data:
            media['lesson'] = pk
        Media.objects.filter(lesson=pk).delete()
        serializer = MediaSerializer(data=data, many=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
