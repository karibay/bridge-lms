from rest_framework import serializers
from .models import Course, Lesson, Media, Submission
from django.utils import timezone

class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = '__all__'

    
class MediaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Media
        fields = ('media_type', 'link', 'lesson')
        extra_kwargs = {
            'lesson': {'write_only': True}
        }

class LessonSerializer(serializers.ModelSerializer):
    media = MediaSerializer(many=True, read_only=True)
    status = serializers.SerializerMethodField()

    class Meta:
        model = Lesson
        fields = '__all__'

    
    def get_status(self, obj):
        user = self.context['request'].user
        now = timezone.now()
        
        if user.has_submitted(obj):
            return 'submitted'
        if not obj.starts_at or not obj.ends_at or obj.starts_at<now:
            return 'in_proccess' if user.has_attendance_in(obj) else 'available'
        if obj.starts_at > now:
            return 'blocked'
        return 'failed' 


class CourseDeatailSerializer(CourseSerializer):
    lessons = LessonSerializer(many=True)


class SubmissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Submission
        fields = '__all__'

