# Generated by Django 2.1.7 on 2019-04-02 09:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0008_auto_20190402_1551'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='lesson',
            name='tasks_count',
        ),
        migrations.AddField(
            model_name='lesson',
            name='modified_at',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
