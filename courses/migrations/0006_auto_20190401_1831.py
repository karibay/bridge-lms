# Generated by Django 2.1.7 on 2019-04-01 12:31

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0005_attendance'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attendance',
            name='lesson',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='attendances', to='courses.Lesson'),
        ),
    ]
