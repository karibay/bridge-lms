# Generated by Django 2.1.7 on 2019-04-02 09:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0007_auto_20190401_1923'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lesson',
            name='ends_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='lesson',
            name='starts_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
