from django.db import models

class Course(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    image = models.ImageField()
    is_available = models.BooleanField(default=True)

    def __str__(self):
        return self.title

class Lesson(models.Model):
    course = models.ForeignKey('Course', on_delete=models.CASCADE, related_name='lessons')
    title = models.CharField(max_length=255)
    description = models.TextField()
    image = models.ImageField()
    
    starts_at = models.DateTimeField(blank=True, null=True)
    ends_at = models.DateTimeField(blank=True, null=True)
    
    tasks = models.TextField()
    task_file = models.FileField(blank=True, null=True)
    submit_file = models.BooleanField(default=False)
    
    modified_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title


class Media(models.Model):
    lesson = models.ForeignKey('Lesson', on_delete=models.CASCADE, related_name='media')
    AUDIO = 'audio'
    VIDEO = 'video'
    MEDIA_TYPES = ((AUDIO, 'Audio'), (VIDEO, 'Video'))
    media_type = models.CharField(max_length=6, choices=MEDIA_TYPES)
    link = models.URLField()

    def __str__(self):
        return self.link

class Submission(models.Model):
    user = models.ForeignKey('accounts.User', on_delete=models.CASCADE)
    lesson = models.ForeignKey('Lesson', on_delete=models.CASCADE, related_name='submissions')
    attachment = models.FileField()

    def __str__(self):
        return f'{self.user.name}:{self.lesson}'


class Attendance(models.Model):
    user = models.ForeignKey('accounts.User', on_delete=models.CASCADE)
    lesson = models.ForeignKey('Lesson', on_delete=models.CASCADE, related_name='attendances')