from django.shortcuts import render
from rest_framework import status

from rest_framework.mixins import CreateModelMixin, UpdateModelMixin
from rest_framework.viewsets import GenericViewSet
from rest_framework.decorators import action, permission_classes
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework import serializers

from accounts.models import User
from accounts.serializers import UserSerializer, UserUpdateSerializer, ActivationSerializer, RequestCodeSerializer, PasswordResetSerializer, CodeConfirmationSerializer
from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token

from .permissions import IsSelfOrReadOnly

class UserViewSet(GenericViewSet, CreateModelMixin, UpdateModelMixin):
    queryset = User.objects.all()
    permission_classes = (IsSelfOrReadOnly, )

    def get_serializer_class(self):
        if self.action == 'create':
            return UserSerializer
        return UserUpdateSerializer

    def get_object(self):
        if self.kwargs.get('pk', None) == 'me':
            self.kwargs['pk'] = self.request.user.pk
        return super(UserViewSet, self).get_object()


    @action(detail=True, methods=['post'])
    def activate(self, request, pk=None):
        serializer = ActivationSerializer(data=request.data, context={'user': self.get_object()})
        if serializer.is_valid(raise_exception=True):
            user = serializer.save()
            return Response(UserSerializer(user).data, status=200) 


    @action(detail=False, methods=['post'])
    def login(self, request):
        if not 'phone_number' in request.data or not 'password' in request.data:
            msg = 'Please, provide username and password'
            raise serializers.ValidationError(msg, code='authorization')

        user = authenticate(request=request, username=request.data['phone_number'], 
                            password=request.data['password'])
        if not user:
            msg = 'Unable to log in with provided credentials.'
            raise serializers.ValidationError(msg, code='authorization')

        token, _ = Token.objects.get_or_create(user=user)
        return Response({'token': token.key, 'user': UserSerializer(user).data})

    
    @action(detail=False, methods=['get'])
    def me(self, request):
        data = UserUpdateSerializer(request.user).data
        return Response(data)

    @action(detail=False, methods=['post'])
    def request_code(self, request):
        data = request.data
        data['user'] = request.user.pk
        serializer = RequestCodeSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['post'])
    def set_password(self, request):
        data = request.data
        data['user'] = request.user.pk
        serializer = PasswordResetSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['post'])
    def confirm_code(self, request):
        data = request.data
        data['user'] = request.user.pk
        serializer = CodeConfirmationSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


