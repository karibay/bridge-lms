from rest_framework import serializers

from accounts.models import User
from django.contrib.auth.hashers import make_password
from django.shortcuts import get_object_or_404

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'phone_number', 'password', 'user_type')
        extra_kwargs = {
            'password': {'write_only': True}
        }

    def validate_password(self, value):
        return make_password(value)


class UserUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('name', 'gender', 'email', 'birth_date', 'instagram', 'user_type')
        extra_kwargs = {
            'name': {'required': True},
            'email': {'required': True},
            'gender': {'required': True},
            'instagram': {'required': True},
            'birth_date': {'required': True},
        }


class ActivationSerializer(serializers.Serializer):
    code = serializers.CharField()

    def create(self, validated_data):
        self.context['user'].is_active = True
        return self.context['user']

    def validate_code(self, value):
        if value == '4444':
            return value
        raise serializers.ValidationError('Actiovation code is invalid')


class RequestCodeSerializer(serializers.Serializer):
    email = serializers.EmailField(write_only=True)

    def create(self, validated_data):
        from django.core.cache import cache
        from django.shortcuts import get_object_or_404
        user = get_object_or_404(User.objects.all(), email=validated_data['email'])
        code = generate_code()
        cache.set(user.phone_number, code)
        send_code(user.email, code)
        return user


class CodeConfirmationSerializer(serializers.Serializer):
    email = serializers.EmailField(write_only=True)
    code = serializers.CharField(write_only=True)
  
    def validate(self, data):
        from django.core.cache import cache
        from django.shortcuts import get_object_or_404
        user = get_object_or_404(User.objects.all(), email=data['email'])

        if data['code'] == cache.get(user.phone_number):
            return data
        raise serializers.ValidationError('Please, enter valid code')

    def create(self, validated_data):
        from django.core.cache import cache
        from django.shortcuts import get_object_or_404
        user = get_object_or_404(User.objects.all(), email=validated_data['email'])
        password = generate_password()
        user.set_password(password)
        user.save()
        send_code(user.email, f'Телефон:{user.phone_number}\nПароль:{password}')
        return self


class PasswordResetSerializer(serializers.Serializer):
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), write_only=True)
    old_password = serializers.CharField(style={'input_type': 'password', 'placeholder': 'Password'}, write_only=True)
    password = serializers.CharField(style={'input_type': 'password', 'placeholder': 'Password'}, write_only=True)

    def validate(self, data):
        if not data['user'].check_password(data['old_password']):
            raise serializers.ValidationError('Invalid password')
        return data
    
    def create(self, validated_data):
        validated_data['user'].set_password(validated_data['password'])
        validated_data['user'].save()
        return self    

def generate_code():
    import string
    import random
    return ''.join(random.sample(string.digits, 4))

def generate_password():
    import string
    import random
    return ''.join(random.sample(string.digits+string.ascii_letters, 6))

def send_code(email, code):
    from django.core.mail import EmailMultiAlternatives
    msg = EmailMultiAlternatives('Код для обновления', code, 'Shonbay University<sou@zratio.kz>',
                                 [email])
    msg.attach_alternative(code, "text/html")
    msg.send()