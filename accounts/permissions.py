from rest_framework import permissions

ALLOWED_ACTIONS = ('create', 'retrieve', 'activate', 'login', 'request_code', 'confirm_code')

class IsSelfOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        return view.action in ALLOWED_ACTIONS or request.user and request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        if view.action in ALLOWED_ACTIONS:
            return True

        return request.user == obj 