from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import UserViewSet
from courses.views import CoursesViewSet, LessonViewSet
from rest_framework_nested import routers

router = DefaultRouter()
router.register('users', UserViewSet)
router.register('courses', CoursesViewSet)

courses_router = routers.NestedSimpleRouter(router, r'courses', lookup='course')
courses_router.register(r'lessons', LessonViewSet, base_name='course-lessons')

urlpatterns = [
    path(r'', include(router.urls)),
    path(r'', include(courses_router.urls))
]
