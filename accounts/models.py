from django.db import models
from django.contrib.auth.models import AbstractBaseUser,BaseUserManager
from django.contrib.auth.models import PermissionsMixin

# Create your models here.
class UserManager(BaseUserManager):
    use_in_migrations = True
    
    def _create_user(self, phone_number, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not phone_number:
            raise ValueError('The given phone number must be set')
        user = self.model(phone_number=phone_number, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user
    
    def create_user(self, phone_number, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(phone_number, password, **extra_fields)

    def create_superuser(self, phone_number, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_staff', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(phone_number, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(unique=True, blank=True, null=True)
    phone_number = models.CharField(max_length=15, unique=True)
    
    name = models.CharField(max_length=255, blank=True, default='')
    
    MALE = 'male'
    FEMALE = 'female'
    GENDERS = ((MALE, 'Male'), (FEMALE, 'Female'))
    gender = models.CharField(max_length=6, choices=GENDERS, blank=True)
    
    birth_date = models.DateField(blank=True, null=True)
    instagram = models.CharField(max_length=255, blank=True, default='')
    
    is_staff = models.BooleanField(default=False)
    
    USERNAME_FIELD = 'phone_number'

    objects = UserManager()

    def __str__(self):
        return self.phone_number

    def user_type(self):
        return 'admin' if self.is_staff else 'student'

    def has_attendance_in(self, lesson):
        return lesson.attendances.filter(user=self).exists()

    def has_submitted(self, lesson):
        return lesson.submissions.filter(user=self).exists()
    